class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :sender_id
      t.integer :recipent_id
      t.text :message
      t.string :status
      t.string :conversation_id

      t.timestamps
    end
  end
end
