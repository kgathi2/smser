class MessagesController < ApplicationController
  before_filter :find_message, :only => [:show, :edit, :update, :destroy]

  # GET /messages
  # GET /messages.xml
  def index
    @messages = Message.all

    respond_to do |wants|
      wants.html # index.html.erb
      wants.xml  { render :xml => @messages }
    end
  end

  # GET /messages/1
  # GET /messages/1.xml
  def show
    respond_to do |wants|
      wants.html # show.html.erb
      wants.xml  { render :xml => @message }
    end
  end

  # GET /messages/new
  # GET /messages/new.xml
  def new
    @message = Message.new

    respond_to do |wants|
      wants.html # new.html.erb
      wants.xml  { render :xml => @message }
    end
  end

  # GET /messages/1/edit
  def edit
  end

  # POST /messages
  # POST /messages.xml
  def create
    @message = Message.new(params[:message])

    respond_to do |wants|
      if @message.save
        flash[:notice] = 'Message was successfully created.'
        wants.html { redirect_to(@message) }
        wants.xml  { render :xml => @message, :status => :created, :location => @message }
      else
        wants.html { render :action => "new" }
        wants.xml  { render :xml => @message.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /messages/1
  # PUT /messages/1.xml
  def update
    respond_to do |wants|
      if @message.update_attributes(params[:message])
        flash[:notice] = 'Message was successfully updated.'
        wants.html { redirect_to(@message) }
        wants.xml  { head :ok }
      else
        wants.html { render :action => "edit" }
        wants.xml  { render :xml => @message.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.xml
  def destroy
    @message.destroy

    respond_to do |wants|
      wants.html { redirect_to(messages_url) }
      wants.xml  { head :ok }
    end
  end

  private
    def find_message
      @message = Message.find(params[:id])
    end

end
